package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.List;

public class ChainSpell implements Spell {
    List<Spell> listSpell;
    public ChainSpell(List<Spell> spells) {
        listSpell=spells;
    }
    // TODO: Complete Me

    @Override
    public void cast() {
        for (Spell spell :
                listSpell) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = listSpell.size()-1; i >= 0 ; i--) {
            listSpell.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
